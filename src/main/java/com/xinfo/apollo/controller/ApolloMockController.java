package com.xinfo.apollo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author chenfoxin
 * @Description 测试mock数据
 * @Date 2022/3/19 22:38
 **/
@RestController
@RequestMapping("/apolloMockController")
public class ApolloMockController {

    @Value("${spring.demo.apollo:0}")
    private String value;

    @GetMapping("/mock")
    public String mock(){
        return value;
    }
}
