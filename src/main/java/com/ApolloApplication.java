package com;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableApolloConfig
public class ApolloApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext context =  SpringApplication.run(ApolloApplication.class, args);
	}
}
